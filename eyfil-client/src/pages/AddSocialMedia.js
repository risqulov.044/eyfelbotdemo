import React, {Component} from 'react';
import Navbar from "../components/Navbar";

class AddSocialMedia extends Component {
    render() {
        return (
            <div className={'container mt-3'}>
                <div className="row">
                    <div className="col-md-3">
                        <Navbar activePage={'4'}/>
                    </div>
                    <div className="col-md-9">
                        <h1>Add Social Media</h1>
                    </div>
                </div>
            </div>
        );
    }
}

export default AddSocialMedia;