import React, {Component} from 'react';
import Navbar from "../components/Navbar";

class AddProduct extends Component {
    render() {
        return (
            <div className={'container mt-3'}>
                <div className="row">
                    <div className="col-md-3">
                        <Navbar activePage={'3'}/>
                    </div>
                    <div className="col-md-9">
                        <h1>Add Product</h1>
                    </div>
                </div>
            </div>
        );
    }
}

export default AddProduct;