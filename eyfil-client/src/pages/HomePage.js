import React, {Component} from 'react';
import {AvForm, AvField} from 'availity-reactstrap-validation';
import axios from "axios";
import {BaseUrl, TOKEN, TOKEN_TYPE, userMe} from "../utils/constants";
import {apiUrl} from "../Api/api";
import {toast} from "react-toastify";

class HomePage extends Component {

    render() {
        const login = (e, v) => {
            axios.post(BaseUrl + apiUrl.login, v).then(res => {
                    if (res.status === 200) {
                        console.log(res.data)
                        localStorage.setItem(
                            TOKEN, TOKEN_TYPE + res.data.object
                        )

                        toast.success('Muvofaqqiyatli')
                       this.props.history.push('/dashBoard')
                    }
                }
            )
                .catch(res=>{
                    toast.error('Login yoki parol xato')
                })
        }
        return (
            <div>
                    <div className="row">
                        <div className="col-md-5">
                            <AvForm onValidSubmit={login}>
                                <AvField label={'Loginingizni kiriting:'} type={'text'} name={'username'}/>
                                <AvField label={'Parolingizni kiriting:'} type={'password'} name={'password'}/>
                                <button className={'btn btn-success'} type={'submit'}>
                                    Kirish
                                </button>
                            </AvForm>
                        </div>
                        <div className="col-md-7">
                            <img className={"imgStyle"} src={"files/eyfel-brand-image.jpg"} alt=""/>
                        </div>
                    </div>

            </div>
        );
    }
}


export default HomePage;