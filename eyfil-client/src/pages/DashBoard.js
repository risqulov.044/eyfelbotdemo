import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import Navbar from "../components/Navbar";
import {BaseUrl, TOKEN, TOKEN_TYPE} from "../utils/constants";
import Requests from "../utils/Requests";
import {toast} from "react-toastify";


class DashBoard extends Component {
    componentDidMount(){
       const token=localStorage.getItem(TOKEN)
        const headers={
            'Authorization': token,
            'Access-Control-Allow-Origin': '*'
        }
        console.log(headers,'HEADERS')
       Requests.me().then(res=>{
           console.log(res,'RESRESRES')
           if (res.status === 200) {
               if (res.data){

               }else {
                   localStorage.removeItem(TOKEN)
                   this.props.history.push('/')
               }

           }else {
               localStorage.removeItem(TOKEN)
               this.props.history.push('/')
           }
       })
    }

    render() {
        return (
            <div className={'container mt-3'}>
                <div className="row">
                    <div className="col-md-3">
                        <Navbar activePage={'1'}/>
                    </div>
                    <div className="col-md-9">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
        );
    }
}

export default DashBoard;