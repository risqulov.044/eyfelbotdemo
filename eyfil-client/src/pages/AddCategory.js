import React, {Component} from 'react';
import Navbar from "../components/Navbar";
import axios from "axios";
import {BaseUrl, TOKEN} from "../utils/constants";
import {apiUrl} from "../Api/api";
import Requests from "../utils/Requests";
import Loading from "../components/Loading";
import {ModalBody, ModalFooter, ModalHeader, Modal} from "reactstrap";
import {toast} from "react-toastify";

class AddCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            isLoading: true,
            openModal: false,
            catName:'',
            item:'',
            deleteModal:false
        }
    }

    componentDidMount() {
        const token = localStorage.getItem(TOKEN)
        const headers = {
            'Authorization': token,
            'Access-Control-Allow-Origin': '*'
        }
        console.log(headers, 'HEADERS')
        Requests.me().then(res => {
            console.log(res, 'RESRESRES')
            if (res.status === 200) {
                if (res.data) {

                } else {
                    localStorage.removeItem(TOKEN)
                    this.props.history.push('/')
                }

            } else {
                localStorage.removeItem(TOKEN)
                this.props.history.push('/')
            }
        })
        Requests.getAllCategories(

        ).then(res => {
           this.setState({
                categories: res,
                isLoading: !this.state.isLoading
            })
        })
    }

    add = (item) => {
        this.setState({
            openModal:!this.state.openModal,
            item
        })
    }

    getName=(e)=>{
        this.setState({
            catName:e.target.value
        })
    }

    addCategory=()=>{
        const {catName, item} = this.state
        if(catName==='') {
            alert("Kategoriya nomi bo'sh bo'lmasligi kerak")
        } else {
            let data={}
            if(item) {
                data.id=item.id
            }
            data.name=catName
            Requests.addCategory(data).then(response=>{
                if(response.data.succed) {
                    Requests.getAllCategories(
                    ).then(res => {
                        this.setState({
                            categories: res,
                            openModal:!this.state.openModal
                        })
                    })
                    toast.success(response.data.message)
                }  else {
                    toast.error(response.data.message)
                }

            })
        }
    }

    delete=(item)=>{
        this.setState({
            deleteModal:!this.state.deleteModal,
            item
        })
    }

    remove=()=>{
        Requests.deleteCategory(this.state.item.id).then(response=>{
            if(response.data.succed) {
                Requests.getAllCategories(
                ).then(res => {
                    this.setState({
                        categories: res,
                        deleteModal:!this.state.deleteModal
                    })
                })
                toast.success(response.data.message)
            } else {
                toast.error(response.data.message)
            }
        })
    }

    render() {
        return (
            <div>
                <Modal isOpen={this.state.openModal} toggle={this.add}>
                    <ModalHeader>
                        <h1>Add Category</h1>
                    </ModalHeader>
                    <ModalBody>
                        <input defaultValue={this.state.item?this.state.item.name:''} type="text" placeholder={'Enter name'} onChange={(e)=>this.getName(e)}/>
                    </ModalBody>
                    <ModalFooter>
                        <button className={'btn btn-success'} onClick={this.addCategory}>Add</button>
                        <button className={'btn-btn-warning'} onClick={()=>this.add('')}>Cancel</button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.deleteModal} toggle={()=>this.delete('')}>
                    <ModalHeader>
                        <h1>Are you going to delete category?</h1>
                    </ModalHeader>
                    <ModalFooter>
                        <button className={'btn btn-success'} onClick={this.remove}>Delete</button>
                        <button className={'btn-btn-warning'} onClick={()=>this.delete('')}>Cancel</button>
                    </ModalFooter>
                </Modal>

                <div className={'container mt-3'}>
                    {this.state.isLoading ?
                        <Loading/>
                        :
                        <div className="row">
                            <div className="col-md-3">
                                <Navbar activePage={'2'}/>
                            </div>
                            <div className="col-md-9">
                                <h1>Add Category</h1>
                                <div className="row">
                                    <div className="col-md-1 offset-11">
                                        <button className={'btn btn-success'} onClick={()=>this.add('')}>Add</button>
                                    </div>
                                </div>
                                <table className={'table'}>
                                    <thead>
                                    <tr>
                                        <th>T/r</th>
                                        <th>Name</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {this.state.categories.map((item, index) =>
                                        <tr>
                                            <td>{index + 1}</td>
                                            <td>{item.name}</td>
                                            <td>
                                                <button className={'btn btn-warning'} onClick={()=>this.add(item)}>Edit</button> {' '}
                                                <button className={'btn btn-danger'} onClick={()=>this.delete(item)}>Delete</button>
                                            </td>

                                        </tr>
                                    )}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    }

                </div>
            </div>
        );
    }
}


                export default AddCategory;