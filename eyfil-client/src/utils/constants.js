import axios from "axios";


export const BaseUrl='http://localhost:8080/api/'
// export const BaseUrl='/api/'
export const TOKEN='EyfelToken'
export const TOKEN_TYPE='Bearer '
export const configHeader=({headers:{
        'Authorization':localStorage.getItem(TOKEN),
        'Access-Control-Allow-Origin': '*'
    }})
export const request=(method,url,data)=>{
    const token=localStorage.getItem(TOKEN)
    const headers={
        'Authorization':token,
        'Access-Control-Allow-Origin': '*'
    }
    return axios({
        url:BaseUrl+url,method,data,headers})
}