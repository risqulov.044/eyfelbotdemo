import {request} from "./constants";
import {apiUrl} from "../Api/api";

class Requests{
    static async me(){
        let response
       await request('get',apiUrl.me).then(res=>{
           response=res
       }).catch(res=>{
           response=res
       })
        return response
    }
    static async getAllCategories(){
        let allCategories=[]
        await request('get',apiUrl.allCategories).then(res=>{
            allCategories=res.data
        })
        return allCategories
    }


    static async addCategory(data) {
       let response =  await request('post', apiUrl.allCategories, data)
        return response
    }

    static async deleteCategory(id) {
        let response = await request('delete', apiUrl.allCategories+"/"+id)
        return response
    }
}

export default Requests