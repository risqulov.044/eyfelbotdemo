import React from 'react'
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import HomePage from "./pages/HomePage";
import DashBoard from "./pages/DashBoard";
import AddCategory from "./pages/AddCategory";
import Navbar from "./components/Navbar";
import AddProduct from "./pages/AddProduct";
import AddSocialMedia from "./pages/AddSocialMedia";
import AddAboutUs from "./pages/AddAboutUs";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {createBrowserHistory} from 'history'

const history=createBrowserHistory()

const App=()=> {
    return(
        <div>
      <BrowserRouter>
          <ToastContainer/>
        <Switch>
            <Route path='/dashBoard' exact component={DashBoard}/>
            <Route path='/addCategory' exact component={AddCategory}/>
            <Route path='/addProduct' exact component={AddProduct}/>
            <Route path='/socialMedia' exact component={AddSocialMedia}/>
            <Route path='/addSocialMedia' exact component={AddSocialMedia}/>
            <Route path='/addAboutUs' exact component={AddAboutUs}/>
            <Route path='/' history={history} exact component={HomePage}/>
        </Switch>
      </BrowserRouter>
        </div>
      )
}
export default App