import React, {useState} from 'react';
import {Menu} from 'antd';


const Navbar=({activePage})=>{
    return(
        <div>
            <Menu mode="inline" style={{ width: 256 }}  defaultSelectedKeys={['1']} selectedKeys={activePage}>
                    <Menu.Item key="1" ><a href="/">Main page</a></Menu.Item>
                    <Menu.Item key="2" ><a href="/addCategory">Add Category</a></Menu.Item>
                    <Menu.Item key="3"><a href="/addProduct">Add Product</a></Menu.Item>
                    <Menu.Item key="4"><a href="/addSocialMedia">Social Media</a></Menu.Item>
                    <Menu.Item key="5"><a href="addAboutUs">About Us</a></Menu.Item>
                    <Menu.Item key="6"><a href="/">Logout</a></Menu.Item>
            </Menu>
        </div>
    )
}
export default Navbar