import React from "react";
import ReactDom from 'react-dom'
import App from './App'
import './index.css'
import 'antd/dist/antd.min.css'

ReactDom.render(
    <React.StrictMode>
        <App/>
    </React.StrictMode>
    ,document.getElementById('root')
)